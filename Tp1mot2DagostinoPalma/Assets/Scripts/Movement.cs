﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    public Rigidbody2D rb;
    public float moveSpeed = 10f;
    public float jumpForce = 10f;
    public Transform groundCheck;
    public LayerMask Ground;
    public float groundRadius = 0.2f;
    bool isGrounded = false;

    float mx = 0f;
    bool isFacingRight = true;




    void Update()
    {
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }  
    }

    private void FixedUpdate()
    {
        mx = Input.GetAxis("Horizontal");

        if(mx < 0)
        {
            isFacingRight = false;
            transform.localScale = new Vector2(-1, transform.localScale.y);
        }
        else
        {
            isFacingRight = true;
            transform.localScale = new Vector2(1, transform.localScale.y);
        }

        rb.velocity = new Vector2(mx * moveSpeed, rb.velocity.y);

        bool touchingGround = Physics2D.OverlapCircle(groundCheck.position, groundRadius, Ground);

        if (touchingGround)
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }

    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }



}
